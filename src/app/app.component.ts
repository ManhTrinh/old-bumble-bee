import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Price } from './models/index';
import { AppService, IMessage } from './app.service';
declare var $: any;
import { OwlCarousel } from 'ngx-owl-carousel';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  message: IMessage = {};
  prices: FirebaseListObservable<any[]>;
  testimonials: FirebaseListObservable<any[]>;
  @ViewChild('owlElement') owlElement: OwlCarousel;
  @ViewChild('owlElement2') owlElement2: OwlCarousel;

  constructor (
    private appService: AppService,
    db: AngularFireDatabase
  ) {
    this.prices = db.list('prices');
    this.testimonials = db.list('testimonials');
  }

  ngAfterViewInit(): void {
    this.owlElement.next([200]);
    setTimeout(function() {
      this.fun();
    }, 200);
  }

  fun() {
    this.owlElement2.next([200]);
  }

  sendEmail(message: IMessage) {
    this.appService.sendEmail(message).subscribe(res => {
      console.log('AppComponent Success', res);
    }, error => {
      console.log('AppComponent Error', error);
    })
  }
}
