export class Price {
  name: string;
  standard_price: number;
  post_construction_price_a: number;
  post_construction_price_b: number;
}
