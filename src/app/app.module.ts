import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppService } from './app.service';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from '../environments/environment';
import * as firebase from 'firebase/app';

// Helpers
import { FlexLayoutModule } from '@angular/flex-layout';
import { MdIconModule, MdInputModule, MdButtonModule } from '@angular/material';
import { OwlModule } from 'ngx-owl-carousel';
import 'hammerjs';

// App Components
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase, 'bumble-bee'),
    AngularFireDatabaseModule,
    // Helpers
    FlexLayoutModule,
    MdIconModule,
    MdInputModule,
    MdButtonModule,
    OwlModule
  ],
  providers: [
    AppService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
