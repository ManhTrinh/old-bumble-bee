// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDx0QBem3Ky-uyAEOgrrmozeWT5_rH48YA',
    authDomain: 'bumble-bee-9bc7e.firebaseapp.com',
    databaseURL: 'https://bumble-bee-9bc7e.firebaseio.com',
    projectId: 'bumble-bee-9bc7e',
    storageBucket: 'bumble-bee-9bc7e.appspot.com',
    messagingSenderId: '630716716227'
  }
};
