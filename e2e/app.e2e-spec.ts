import { BumbleBeePage } from './app.po';

describe('bumble-bee App', () => {
  let page: BumbleBeePage;

  beforeEach(() => {
    page = new BumbleBeePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
